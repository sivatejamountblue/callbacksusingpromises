const fs = require('fs');




function createDictionary(filepath){
    return new Promise((resolve,reject)=>{
        fs.mkdir(filepath, ((err)=>{
            if (err){
                reject(err)
            }
            else{
                resolve("Created dictionary successfully")
            }
        }))
    })
}

function writefile(filepath) {
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            fs.writeFile(filepath, 'Hi', (err) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve("Its completed")
                }
            })
        },1000)
    })
}
function deletefile(filepath) {

    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            fs.unlink(filepath, (err) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve("Its Removed")
                }
            })
        },1000)
    })
}
function deleteDictionary(filepath){
    return new Promise((resolve,reject)=>{
        fs.rmdir(filepath, ((err)=>{
            if (err){
                reject(err)
            }
            else{
                resolve("Deleted dictionary successfully")
            }
        }))
    })
}

function makingdirectory(dire){
    let prm1 = Promise.resolve()
    createDictionary(dire)
    return prm1
}


function generateFiles(n) {
    let prm2 = Promise.resolve()
    for (let i = 1; i < n; i++) {
        prm2 = prm2.then(() => {
            return writefile(`./random/testfile${i}.txt`)
        })
    }
    return prm2
}

function deletefiles(n) {
    let prm3 = Promise.resolve()
    for (let i = 1; i < n; i++) {
        prm3 = prm3.then(() => {
            return deletefile(`./random/testfile${i}.txt`)
        })
    }
    return prm3
}
function deletingDirectory(dire){
    let prm4 = Promise.resolve()
    deleteDictionary(dire)
    return prm4
}


module.exports.makingdirectory = makingdirectory;
module.exports.generateFiles = generateFiles;
module.exports.deletefiles = deletefiles;
module.exports.deletingDirectory = deletingDirectory;
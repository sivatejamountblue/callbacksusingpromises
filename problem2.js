const fs = require('fs');

function newPromise(filePath){
    return new Promise(function(resolve, reject){
        fs.readFile(filePath, 'utf-8', function(err,data){
            if (err){
                reject(err)
            }
            else{
                resolve(data)
            }
        })
        
    }) 
}
function writefileFunction(filepath1, data){
    return new Promise((resolve,reject)=>{
        fs.writeFile(filepath1, data, function(err){
            if (err){
                reject(err)
            }
            else{
                resolve(data)
            }
        })
    })
}

function appendfileFunction(filepath2, data){
    return new Promise((resolve, reject)=>{
        fs.appendFile(filepath2, data +'\n', function(err){
            if (err){
                reject(err)
            }
            else{
                resolve(data)
            }
        })
    })
}

function unLink(filepath3){
    for (let each of filepath3.split('\n')){
        if (each.length > 0){
            fs.unlink(each, function(err){
                if (err){
                    throw(err)
                }
            })
        } 
    }
    
}


module.exports.newPromise = newPromise;
module.exports.writefileFunction = writefileFunction;
module.exports.appendfileFunction = appendfileFunction;
module.exports.unLink = unLink;
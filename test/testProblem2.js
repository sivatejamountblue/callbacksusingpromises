const test = require('../problem2');
const path = require('path');

const dataPath = path.resolve(__dirname,'../lipsum.txt');
const uppercasePath = path.resolve(__dirname, 'upperCase.txt');
const lowercasePath = path.resolve(__dirname, 'lowerCase.txt');
const sortedFilePath = path.resolve(__dirname, 'sortedFile.txt');
const filenamePath1 = path.resolve(__dirname, 'filename.txt');


test.newPromise(dataPath)
.then((data) => {
    console.log(data)
    test.writefileFunction(uppercasePath, data.toUpperCase())
    test.appendfileFunction(filenamePath1, uppercasePath)
    return test.newPromise(uppercasePath)  
})
.then((upperCaseData) => {  
    console.log(upperCaseData)
    test.writefileFunction(lowercasePath, JSON.stringify(upperCaseData.toLowerCase().split('.')));
    test.appendfileFunction(filenamePath1, lowercasePath)
    return test.newPromise(lowercasePath)
})
.then((lowerCaseData) => { 
    test.writefileFunction(sortedFilePath, JSON.stringify(JSON.parse(lowerCaseData).sort()));
    test.appendfileFunction(filenamePath1,sortedFilePath)
    return test.newPromise(filenamePath1 )
})
.then((filenamePath) => {
    console.log(filenamePath)
    test.unLink(filenamePath)
    test.writefileFunction(filenamePath1, '')
})
